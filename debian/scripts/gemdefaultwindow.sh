#!/bin/sh

INFILE=$1
shift

OUTFILE=${INFILE##*/}
OUTFILE=${OUTFILE%.in}
OUTFILE=${OUTFILE%.pd}

for WINFILE in "$@"; do
  WINFILE=${WINFILE##*/}
  WINFILE=${WINFILE%.*}
  BACKEND=${WINFILE%window}
  BACKEND=${BACKEND#gem}
  cat "${INFILE}" | sed -e "s|@GEM_DEFAULT_WINDOW@|${WINFILE}|g" > "${OUTFILE}-${BACKEND}.pd"
done
